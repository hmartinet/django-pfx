# Django PFX

[![pipeline status](https://gitlab.com/pfx4/django-pfx/badges/master/pipeline.svg)](https://gitlab.com/pfx4/django-pfx/-/commits/master)
[![coverage report](https://gitlab.com/pfx4/django-pfx/badges/master/coverage.svg)](https://pfx4.gitlab.io/django-pfx/coverage)

![logo]

Django PFX is a toolkit designed to streamline the development of RESTful APIs
using the Django framework, leveraging views and the ORM (Object-Relational Mapping).
With a focus on simplicity and efficiency, Django PFX enables developers to rapidly
create robust web service APIs for web applications and simplifies the testing process.

> From version 1.3 PFX is only compatible with Django 4.2 LTS,
> please use version 1.2 for older Django versions.

### Key Features:
- Predefined CRUD API (but customizable)
- Internationalization
- Authentication & Authorization
- JSON-only RESTful API
- Flexibility
- Tests & Profiling Tools
- Automatic Open API Documentation

### Links
- [Source](https://gitlab.com/pfx4/django-pfx)
- [Issue tracker](https://gitlab.com/pfx4/django-pfx/-/issues)
- [Documentation](https://pfx4.gitlab.io/django-pfx/doc/)

[logo]: https://gitlab.com/pfx4/django-pfx/-/raw/master/img/pfx.png "PFX"
