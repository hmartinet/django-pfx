

class UserFilteredQuerySetMixin:
    def user(self, user):
        raise NotImplementedError("Method user is not defined!")
