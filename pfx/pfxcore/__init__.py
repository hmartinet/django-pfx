from .apps import PfxAppConfig
from .shortcuts import register_views

default_app_config = 'pfx.pfxcore.apps.PfxCoreConfig'

__PFX_VIEWS__ = []
